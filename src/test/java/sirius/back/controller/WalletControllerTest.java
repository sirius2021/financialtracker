package sirius.back.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import sirius.back.constants.TransactionType;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.UserEntity;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;
import sirius.back.repository.CategoryRepository;
import sirius.back.repository.TransactionRepository;
import sirius.back.repository.WalletRepository;
import sirius.back.service.UserService;
import sirius.back.utils.CategoryTestUtils;
import sirius.back.utils.TransactionUtils;
import sirius.back.utils.WalletUtils;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureWebMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest()
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class WalletControllerTest {

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserService userService;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private static String email;
    private static String authToken;
    private static MultiValueMap<String, String> headers;
    private WalletUtils walletUtils;

    @BeforeAll
    public static void defineVariables() {
        email = "123";
        authToken = "Bearer 123";

        // request headers
        headers = new LinkedMultiValueMap<>();
        headers.put("Email", List.of(email));
        headers.put("Authorization", List.of(authToken));
    }

    @BeforeEach
    public void setUp() {
        walletUtils = new WalletUtils();
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void getWalletsTest() throws Exception {
        long walletId = 1;

        this.mockMvc.perform(get("/wallets").headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(convertObjectsToJson(List.of(walletUtils.getDefaultWallet(walletId)))));
    }

    @Test
    public void createWallet() throws Exception {
        UserEntity user = userService.createUser(email, authToken);

        this.mockMvc.perform(post("/wallet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJson(walletUtils.getWallet()))
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isOk());


        List<WalletEntity> wallets = walletRepository.findAllByUser(user);
        assertEquals(1, wallets.size());
        assertEquals(walletUtils.getWalletEntity(), wallets.get(0));
    }

    @Test
    public void createWalletWithoutName() throws Exception {
        this.mockMvc.perform(post("/wallet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJson(walletUtils.getWalletWithoutName()))
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createWalletWithEmptyName() throws Exception {
        this.mockMvc.perform(post("/wallet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJson(walletUtils.getWalletWithEmptyName()))
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createWalletWithoutCurrency() throws Exception {
        this.mockMvc.perform(post("/wallet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJson(walletUtils.getWalletWithoutCurrency()))
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createWalletWithEmptyCurrency() throws Exception {
        this.mockMvc.perform(post("/wallet")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJson(walletUtils.getWalletWithEmptyCurrency()))
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteWallet() throws Exception {
        UserEntity user = userService.createUser(email, authToken);
        WalletEntity wallet = walletRepository.save(walletUtils.getWalletEntity(user));

        this.mockMvc.perform(delete("/wallet/" + wallet.getId())
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isOk());

        assertEquals(0, walletRepository.findAllByUser(user).size());
    }

    @Test
    public void deleteWalletWithTransactions() throws Exception{
        UserEntity user = userService.createUser(email, authToken);
        WalletEntity wallet = walletRepository.save(walletUtils.getWalletEntity(user));
        CategoryEntity category = categoryRepository.save(CategoryTestUtils.getCategoryEntity(1, user, TransactionType.INCOME));
        transactionRepository.save(TransactionUtils.getTransactionEntity(1, wallet, category,
                TransactionType.INCOME, BigDecimal.TEN));

        this.mockMvc.perform(delete("/wallet/" + wallet.getId())
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isOk());

        assertEquals(0, walletRepository.findAllByUser(user).size());
    }

    @Test
    public void getWallet() throws Exception {
        UserEntity user = userService.createUser(email, authToken);
        WalletEntity wallet = walletRepository.save(walletUtils.getWalletEntity(user));
        Wallet expectedWallet = walletUtils.getWallet();
        expectedWallet.setId(wallet.getId());

        this.mockMvc.perform(get("/wallet/" + wallet.getId())
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(convertObjectToJson(expectedWallet)));
    }

    @Test
    public void putWallet() throws Exception {
        UserEntity user = userService.createUser(email, authToken);
        WalletEntity wallet = walletRepository.save(walletUtils.getWalletEntity(user));
        walletUtils.randomize();
        Wallet newWallet = walletUtils.getWallet();
        WalletEntity expectedWallet = walletUtils.getWalletEntity(wallet.getId());

        this.mockMvc.perform(put("/wallet/" + wallet.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(convertObjectToJson(newWallet))
                        .headers(new HttpHeaders(headers)))
                .andDo(print())
                .andExpect(status().isOk());

        assertEquals(expectedWallet, walletRepository.findAllByUser(user).get(0));
    }

    private String convertObjectToJson(Object o) throws JsonProcessingException {
        ObjectMapper ob = new ObjectMapper();
        return ob.writeValueAsString(o);
    }

    private String convertObjectsToJson(List<Object> objects) throws JsonProcessingException {
        ObjectMapper ob = new ObjectMapper();
        return ob.writeValueAsString(objects);
    }

}
