package sirius.back.utils;

import sirius.back.constants.TransactionType;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.TransactionEntity;
import sirius.back.entity.WalletEntity;

import java.math.BigDecimal;

public class TransactionUtils {

    public static TransactionEntity getTransactionEntity(long id, WalletEntity wallet, CategoryEntity category,
                                                         TransactionType transactionType, BigDecimal amount) {
        return TransactionEntity.builder()
                .id(id)
                .wallet(wallet)
                .category(category)
                .type(transactionType)
                .amount(amount)
                .build();
    }
}
