package sirius.back.utils;

import sirius.back.constants.TransactionType;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.UserEntity;

public class CategoryTestUtils {

    public static CategoryEntity getCategoryEntity(long id, UserEntity user, TransactionType transactionType) {
        return CategoryEntity.builder()
                .id(id)
                .name(StringUtils.generateString())
                .type(transactionType)
                .user(user)
                .build();
    }
}
