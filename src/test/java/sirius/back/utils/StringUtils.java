package sirius.back.utils;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class StringUtils {

    private static final int MAX_STRING_LENGTH = 50;

    public static String generateString(){
        byte[] array = new byte[MAX_STRING_LENGTH];
        new Random().nextBytes(array);
        return new String(array, StandardCharsets.UTF_8);
    }

}
