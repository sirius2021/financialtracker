package sirius.back.utils;

import sirius.back.entity.UserEntity;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;

import java.math.BigDecimal;
import java.util.Random;

public class WalletUtils {

    private static final BigDecimal DEFAULT_WALLET_LIMIT = BigDecimal.ZERO;
    private static final boolean DEFAULT_WALLET_VISIBILITY = true;
    private static final String DEFAULT_WALLET_NAME = "new wallet";
    private static final String DEFAULT_WALLET_CURRENCY = "RUB";

    private boolean walletVisibility;
    private BigDecimal walletLimit;
    private String walletCurrency;
    private final Random random;
    private String walletName;

    public WalletUtils() {
        random = new Random();
        walletCurrency = DEFAULT_WALLET_CURRENCY;
        walletVisibility = DEFAULT_WALLET_VISIBILITY;
        walletName = DEFAULT_WALLET_NAME;
        walletLimit = DEFAULT_WALLET_LIMIT;
    }

    public void randomize() {
        walletVisibility = random.nextBoolean();
        walletLimit = BigDecimal.valueOf(Math.abs(random.nextLong()));
        walletName = StringUtils.generateString();
        walletCurrency = StringUtils.generateString();
    }

    public Wallet getWallet() {
        return Wallet.builder()
                .name(walletName)
                .currency(walletCurrency)
                .visibility(walletVisibility)
                .limit(walletLimit)
                .income(BigDecimal.ZERO)
                .expense(BigDecimal.ZERO)
                .balance(BigDecimal.ZERO)
                .isLimitReached(false)
                .build();
    }

    public Wallet getWallet(long id) {
        Wallet wallet = this.getWallet();
        wallet.setId(id);
        return wallet;
    }

    public Wallet getWalletWithoutBalanceIncomeExpense(long id) {
        return this.getWallet()
                .setId(id)
                .setBalance(null)
                .setExpense(null)
                .setIncome(null);
    }

    public WalletEntity getWalletEntity(long id) {
        WalletEntity wallet = getWalletEntity();
        wallet.setId(id);
        return wallet;
    }

    public WalletEntity getWalletEntity(UserEntity user) {
        WalletEntity wallet = getWalletEntity();
        wallet.setUser(user);
        return wallet;
    }

    public WalletEntity getWalletEntity() {
        return WalletEntity.builder()
                .name(walletName)
                .currency(walletCurrency)
                .visibility(walletVisibility)
                .walletLimit(walletLimit)
                .build();
    }

    public Wallet getDefaultWallet(long id) {
        return Wallet.builder()
                .id(id)
                .name(DEFAULT_WALLET_NAME)
                .currency(DEFAULT_WALLET_CURRENCY)
                .visibility(DEFAULT_WALLET_VISIBILITY)
                .limit(DEFAULT_WALLET_LIMIT)
                .balance(BigDecimal.ZERO)
                .expense(BigDecimal.ZERO)
                .income(BigDecimal.ZERO)
                .isLimitReached(false)
                .build();
    }

    public Wallet getWalletWithoutName() {
        Wallet wallet = getWallet();
        wallet.setName(null);
        return null;
    }

    public Wallet getWalletWithEmptyName() {
        Wallet wallet = getWallet();
        wallet.setName("");
        return null;
    }

    public Wallet getWalletWithoutCurrency() {
        Wallet wallet = getWallet();
        wallet.setCurrency(null);
        return null;
    }

    public Wallet getWalletWithEmptyCurrency() {
        Wallet wallet = getWallet();
        wallet.setCurrency("");
        return null;
    }
}
