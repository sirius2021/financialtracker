package sirius.back.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;
import sirius.back.utils.WalletUtils;

public class WalletToWalletEntityConverterTest {

    private static WalletUtils walletUtils;
    private static WalletToWalletEntityConverter walletToWalletEntityConverter;

    @BeforeAll
    public static void setUp() {
        walletUtils = new WalletUtils();
        walletToWalletEntityConverter = new WalletToWalletEntityConverter();
    }

    @Test
    public void conversionTest() {
        Wallet wallet = walletUtils.getWallet();

        WalletEntity expectedResult = walletUtils.getWalletEntity();

        Assertions.assertEquals(expectedResult, walletToWalletEntityConverter.convert(wallet));
    }

    @Test
    public void nullPointerExceptionTest() {
        Assertions.assertThrows(NullPointerException.class, () -> walletToWalletEntityConverter.convert(null));
    }

}
