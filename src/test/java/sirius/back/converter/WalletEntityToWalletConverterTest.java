package sirius.back.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;
import sirius.back.utils.WalletUtils;

public class WalletEntityToWalletConverterTest {

    private static WalletUtils walletUtils;
    private static WalletEntityToWalletConverter  walletEntityToWalletConverter;

    @BeforeAll
    public static void setUp() {
        walletUtils = new WalletUtils();
        walletEntityToWalletConverter = new WalletEntityToWalletConverter();
    }

    @Test
    public void conversionTest() {
        long walletId = 1;

        WalletEntity walletEntity = walletUtils.getWalletEntity(walletId);
        Wallet expectedResult = walletUtils.getWalletWithoutBalanceIncomeExpense(walletId);

        Assertions.assertEquals(expectedResult, walletEntityToWalletConverter.convert(walletEntity));
    }

    @Test
    public void nullPointerExceptionTest() {
        Assertions.assertThrows(NullPointerException.class, () -> walletEntityToWalletConverter.convert(null));
    }
}
