package sirius.back.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;
import sirius.back.utils.WalletUtils;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class WalletEntitiesToWalletsConverterTest {

    private static WalletUtils walletUtils;
    private static WalletEntitiesToWalletsConverter walletEntitiesToWalletsConverter;

    @BeforeAll
    public static void setUp() {
        walletUtils = new WalletUtils();
        walletEntitiesToWalletsConverter = new WalletEntitiesToWalletsConverter(new WalletEntityToWalletConverter());
    }

    @Test
    public void conversionTest() {
        long walletId = 1;

        WalletEntity walletEntity = walletUtils.getWalletEntity(walletId);
        Wallet expectedResult = walletUtils.getWalletWithoutBalanceIncomeExpense(walletId);

        assertArrayEquals(List.of(expectedResult).toArray(),
                walletEntitiesToWalletsConverter.convert(List.of(walletEntity)).toArray());
    }

    @Test
    public void nullPointerExceptionTest() {
        Assertions.assertThrows(NullPointerException.class, () -> walletEntitiesToWalletsConverter.convert(null));
    }

}
