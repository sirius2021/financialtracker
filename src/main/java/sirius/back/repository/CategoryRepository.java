package sirius.back.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.CategoryExpense;
import sirius.back.entity.UserEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryEntity, Long> {

    Iterable<CategoryEntity> findAllByUserAndVisibility(UserEntity user, boolean visibility);

    Optional<CategoryEntity> findByUserAndId(UserEntity user, long id);

    @Query(name = "category_expense_query", nativeQuery = true)
    List<CategoryExpense> getCategoryExpense(long userId);
}
