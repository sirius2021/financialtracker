package sirius.back.repository;

import org.springframework.data.repository.CrudRepository;
import sirius.back.entity.CurrencyEntity;

import java.util.List;
import java.util.Optional;

public interface CurrencyRepository extends CrudRepository<CurrencyEntity, Long> {

    Optional<CurrencyEntity> findFirstByNameOrderByDateDesc(String name);

    List<CurrencyEntity> findTop2ByNameOrderByDateDesc(String name);
}
