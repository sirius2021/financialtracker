package sirius.back.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sirius.back.entity.UserEntity;
import sirius.back.entity.WalletEntity;
import sirius.back.entity.WalletExpense;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends CrudRepository<WalletEntity, Long>  {

    List<WalletEntity> findAllByUser(UserEntity user);

    Optional<WalletEntity> findByUserAndId(UserEntity user, Long userId);

    @Query(name = "wallet_monthly_expense_query", nativeQuery = true)
    List<WalletExpense> getWalletMonthlyExpense(LocalDateTime startYearDate, LocalDateTime endYearDate);
}
