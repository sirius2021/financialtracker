package sirius.back.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sirius.back.constants.TransactionType;
import sirius.back.entity.TransactionEntity;
import sirius.back.entity.TransactionTypeSum;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransactionRepository extends PagingAndSortingRepository<TransactionEntity, Long> {

    int countByWalletId(Long walletId);

    int countByCategoryId(long categoryId);

    List<TransactionEntity> findAllByWalletIdAndType(long walletId, TransactionType type);

    @Query(name = "wallet_finance_query", nativeQuery = true)
    List<TransactionTypeSum> findWalletIncomeAndExpense(long walletId);

    @Query(value = "select trs.id, wallet_id, amount, transaction_type, category_id, trs.currency, date" +
            " from transaction trs " +
            "inner join wallet w on w.id = trs.wallet_id " +
            "inner join user_info ui on ui.id = w.user_id" +
            " where trs.id = :trsId and ui.id = :userId",  nativeQuery = true)
    TransactionEntity findByUserIdAndWalletId(@Param("trsId") long trsId, @Param("userId") long userId);

    @Query(value = "select trs.id, wallet_id, amount, transaction_type, category_id, trs.currency, date" +
            " from transaction trs " +
            "inner join wallet w on w.id = trs.wallet_id " +
            "inner join user_info ui on ui.id = w.user_id" +
            " where ui.id = :userId and w.id = :walletId",  nativeQuery = true)
    Page<TransactionEntity> findAllByUserIdAndWalletId(@Param("userId") long userId,
                                                       @Param("walletId") long walletId,
                                                       org.springframework.data.domain.Pageable pageable);

    void removeAllByWalletId(long walletId);

    @Query(value = "SELECT sum(amount) as amount from transaction where  wallet_id = ?1 and date_trunc('month', date) = ?2 and transaction_type = 'EXPENSE';",
            nativeQuery = true)
    BigDecimal getWalletExpenseForCurrentMonth(long walletId, LocalDateTime currentDate);
}
