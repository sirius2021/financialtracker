package sirius.back.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager",
        basePackages = {"sirius.back.repository"}
)
@RequiredArgsConstructor
public class DataSourceConfig {

    @Primary
    @Bean("dataSource")
    public DataSource dataSource(DataSourceProp DataSourceProp) {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(DataSourceProp.getDriverClassName());
        dataSource.setJdbcUrl(DataSourceProp.getJdbcUrl());
        dataSource.setUsername(DataSourceProp.getUsername());
        dataSource.setPassword(DataSourceProp.getPassword());
        dataSource.setMaximumPoolSize(DataSourceProp.getMaximumPoolSize());
        dataSource.setMinimumIdle(DataSourceProp.getMinimumIdle());
        dataSource.setIdleTimeout(DataSourceProp.getIdleTimeout());
        return dataSource;
    }

}
