package sirius.back.service;

import sirius.back.constants.CurrencyType;
import sirius.back.entity.CurrencyEntity;

public interface CurrencyService {

    CurrencyEntity getCurrencyValue(CurrencyType name);

}
