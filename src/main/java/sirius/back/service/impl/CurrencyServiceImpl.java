package sirius.back.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import sirius.back.constants.CurrencyType;
import sirius.back.entity.CurrencyEntity;
import sirius.back.model.moex.MoexCurrencyResponse;
import sirius.back.repository.CurrencyRepository;
import sirius.back.service.CurrencyService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    String MOEX_URL = "https://iss.moex.com/iss/statistics/engines/currency/markets/selt/rates.json?iss.meta=off";
    private final static long TWELVE_HOURS = 12 * 3_600_000; // milliseconds

    private final RestTemplateBuilder restTemplateBuilder;
    private final CurrencyRepository currencyRepository;

    @Override
    public CurrencyEntity getCurrencyValue(CurrencyType name) {
        return currencyRepository.findFirstByNameOrderByDateDesc(name.toString())
                .orElse(null);
    }

    private void setDynamics(CurrencyEntity entity, List<CurrencyEntity> entities) {
        if (entities.size() == 0) {
            entity.setDynamics("NONE");
        } else {
            BigDecimal curRate = entities.get(0).getRate();

            if (curRate.compareTo(entity.getRate()) == 0) {
                entity.setDynamics("NONE");
            } else if (curRate.compareTo(entity.getRate()) < 0) {
                entity.setDynamics("UP");
            } else {
                entity.setDynamics("DOWN");
            }
        }
    }

    private void saveCurrency(MoexCurrencyResponse response, CurrencyType currencyName,
                              String rateFieldName, String dateFieldName) {

        int rateIndex = response.getCbrf().getColumns().indexOf(rateFieldName);
        int dateIndex = response.getCbrf().getColumns().indexOf(dateFieldName);
        BigDecimal rate = BigDecimal.valueOf((double) response.getCbrf().getData().get(0).get(rateIndex));
        LocalDate date = LocalDate.parse((String) response.getCbrf().getData().get(0).get(dateIndex),
                DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        List<CurrencyEntity> entities = currencyRepository.findTop2ByNameOrderByDateDesc(currencyName.toString());
        if (entities.size() == 0 || !entities.get(0).getDate().equals(date)) {

            CurrencyEntity newEntity = CurrencyEntity.builder()
                    .name(currencyName.toString())
                    .date(date)
                    .rate(rate)
                    .build();

            setDynamics(newEntity, entities);

            currencyRepository.save(newEntity);
        }
    }

    private void saveUsd(MoexCurrencyResponse response) {
        this.saveCurrency(response, CurrencyType.USD, "CBRF_USD_LAST", "CBRF_USD_TRADEDATE");
    }

    private void saveEur(MoexCurrencyResponse response) {
        this.saveCurrency(response, CurrencyType.EUR, "CBRF_EUR_LAST", "CBRF_EUR_TRADEDATE");
    }

    public MoexCurrencyResponse getMoexCurrencyRate() {
        return this.restTemplateBuilder.build().getForObject(MOEX_URL, MoexCurrencyResponse.class);
    }

    @Scheduled(fixedRate = TWELVE_HOURS)
    private void updateCurrencyRate() {
        MoexCurrencyResponse response = getMoexCurrencyRate();
        this.saveUsd(response);
        this.saveEur(response);
    }
}
