package sirius.back.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.back.constants.CurrencyType;
import sirius.back.entity.UserEntity;
import sirius.back.entity.WalletEntity;
import sirius.back.entity.WalletExpense;
import sirius.back.model.Session;
import sirius.back.repository.WalletRepository;
import sirius.back.service.TransactionService;
import sirius.back.service.WalletService;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {

    private final String DEFAULT_WALLET_NAME = "new wallet";
    private final boolean DEFAULT_WALLET_VISIBILITY = true;

    private final TransactionService transactionService;
    private final WalletRepository walletRepository;
    private final Session session;

    public void createWallet(WalletEntity wallet) {
        if (wallet.getWalletLimit() == null) {
            wallet.setWalletLimit(BigDecimal.valueOf(0));
        }
        if (wallet.getVisibility() == null) {
            wallet.setVisibility(DEFAULT_WALLET_VISIBILITY);
        }
        wallet.setUser(session.getUser());
        walletRepository.save(wallet);
    }

    @Override
    public WalletEntity getWallet(long id) {
        return walletRepository.findByUserAndId(session.getUser(), id).orElse(null);
    }

    @Override
    @Transactional
    public void removeWallet(long id) {
        walletRepository.findByUserAndId(session.getUser(), id).ifPresent(wallet -> {
            transactionService.deleteTransactionsByWalletId(id);
            walletRepository.delete(wallet);
        });
    }

    @Override
    public List<WalletEntity> getWallets() {
        return walletRepository.findAllByUser(session.getUser());
    }

    @Override
    public void createDefaultWallet(UserEntity user) {
        walletRepository.save(WalletEntity.builder()
                .user(user)
                .name(DEFAULT_WALLET_NAME)
                .visibility(DEFAULT_WALLET_VISIBILITY)
                .currency(CurrencyType.RUB.toString())
                .walletLimit(BigDecimal.ZERO)
                .build());
    }

    @Override
    public void editWallet(long id, WalletEntity newWallet) {
        walletRepository.findByUserAndId(session.getUser(), id)
                .ifPresent(walletEntity -> {
                    if (newWallet.getName() != null) {
                        walletEntity.setName(newWallet.getName());
                    }

                    if (newWallet.getWalletLimit() != null) {
                        walletEntity.setWalletLimit(newWallet.getWalletLimit());
                    }

                    if (newWallet.getCurrency() != null) {
                        walletEntity.setCurrency(newWallet.getCurrency());
                    }

                    if (newWallet.getVisibility() != null) {
                        walletEntity.setVisibility(newWallet.getVisibility());
                    }

                    walletRepository.save(walletEntity);
                });
    }

    @Override
    public List<WalletExpense> getWalletMonthlyExpense(long walletId) {
        return walletRepository.getWalletMonthlyExpense(
                LocalDateTime.now().withDayOfYear(LocalDateTime.MIN.getDayOfYear()),
                LocalDateTime.now().withDayOfYear(LocalDateTime.MAX.getDayOfYear()));
    }
}
