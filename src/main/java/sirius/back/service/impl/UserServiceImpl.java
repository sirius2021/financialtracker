package sirius.back.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.back.entity.UserEntity;
import sirius.back.repository.UserRepository;
import sirius.back.service.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public boolean userExists(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    @Override
    public UserEntity createUser(String email, String token) {
        return userRepository.save(UserEntity.builder()
                .email(email)
                .token(token)
                .build());
    }

    @Override
    public UserEntity findUserByEmail(String email) {
        return userRepository.findByEmail(email).orElse(null);
    }

    @Override
    public UserEntity setUserToken(UserEntity user, String token) {
        user.setToken(token);
        return userRepository.save(user);
    }
}
