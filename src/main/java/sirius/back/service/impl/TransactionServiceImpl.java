package sirius.back.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import sirius.back.constants.CurrencyType;
import sirius.back.entity.CategoryExpense;
import sirius.back.constants.TransactionType;
import sirius.back.entity.TransactionEntity;
import sirius.back.entity.TransactionTypeSum;
import sirius.back.model.Session;
import sirius.back.model.Session;
import sirius.back.repository.CategoryRepository;
import sirius.back.repository.TransactionRepository;
import sirius.back.repository.WalletRepository;
import sirius.back.service.CurrencyService;
import sirius.back.service.TransactionService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final Session session;
    private final TransactionRepository transactionRepository;
    private final WalletRepository walletRepository;
    private final Logger logger = org.slf4j.LoggerFactory.getLogger(TransactionServiceImpl.class);
    private final CurrencyService currencyService;
    private final CategoryRepository categoryRepository;

    @Override
    public void createTransaction(TransactionEntity transaction){
        if (walletRepository.findByUserAndId
                (session.getUser(), transaction.getWallet().getId()).orElse(null) != null) {
            transactionRepository.save(transaction);
            logger.info("create success");
        } else {
            logger.info("create error");
        }
    }

    @Override
    public List<TransactionEntity> getTransactions(int numberOfItems, int pageNumber, Long walletId) {
        Pageable page = PageRequest.of(pageNumber, numberOfItems, Sort.by("date").descending());
        List<TransactionEntity> transactionEntities = transactionRepository.findAllByUserIdAndWalletId(session.getUser().getId(), walletId, page).toList();
        if(!transactionEntities.isEmpty()) {
            logger.info("getting success");
            return transactionEntities;
        } else {
            logger.info("getting error");
            return null;
        }
    }

    @Override
    public void updateTransaction(TransactionEntity newTransaction, Long id) {
        TransactionEntity transactionEntity = transactionRepository.findByUserIdAndWalletId(id, session.getUser().getId());

        if(transactionEntity != null) {
            if (newTransaction.getAmount() != null && newTransaction.getCurrency() == null) {
                transactionEntity.setAmount(newTransaction.getAmount());
            } else if (newTransaction.getCurrency() != null && newTransaction.getAmount() == null) {

                if (transactionEntity.getCurrency() != newTransaction.getCurrency()) {
                    // convert to RUB
                    if (transactionEntity.getCurrency() != CurrencyType.RUB) {
                        BigDecimal rate = currencyService.getCurrencyValue(transactionEntity.getCurrency()).getRate();
                        transactionEntity.setAmount(transactionEntity.getAmount().multiply(rate));
                    }

                    // convert from RUB
                    if (newTransaction.getCurrency() != CurrencyType.RUB) {
                        BigDecimal rate = currencyService.getCurrencyValue(newTransaction.getCurrency()).getRate();
                        BigDecimal division = transactionEntity.getAmount().divide(rate, RoundingMode.DOWN);
                        transactionEntity.setAmount(division);
                    }

                    // set new currency
                    transactionEntity.setCurrency(newTransaction.getCurrency());
                }
            } else if (newTransaction.getCurrency() != null && newTransaction.getAmount() != null) {
                transactionEntity.setAmount(newTransaction.getAmount());
                transactionEntity.setCurrency(newTransaction.getCurrency());
            }
            if (newTransaction.getCategory() != null) {
                transactionEntity.setCategory(newTransaction.getCategory());
            }
            if (newTransaction.getType() != null) {
                transactionEntity.setType(newTransaction.getType());
            }
            if (newTransaction.getDate() != null) {
                transactionEntity.setDate(newTransaction.getDate());
            }
            transactionRepository.save(transactionEntity);
            logger.info("update success");
        } else {
            logger.info("update error");
        }
    }

    @Override
    public void deleteTransaction(Long id) {
        TransactionEntity transactionEntity = transactionRepository.findByUserIdAndWalletId(id, session.getUser().getId());

        if(transactionEntity != null) {
            transactionRepository.delete(transactionEntity);
            logger.info("delete success");
        } else {
            logger.info("delete error");
        }

    }

    @Override
    public void deleteTransactionsByWalletId(long walletId) {
        transactionRepository.removeAllByWalletId(walletId);
    }

    @Override
    public int countTransactionsByWalletId(Long walletId) {
        return transactionRepository.countByWalletId(walletId);
    }

    @Override
    public List<TransactionTypeSum> getWalletIncomeExpense(long walletId) {
        return transactionRepository.findWalletIncomeAndExpense(walletId);
    }

    BigDecimal countWalletTransactions(long walletId, TransactionType transactionType) {
        AtomicReference<BigDecimal> result = new AtomicReference<>(BigDecimal.ZERO);
        transactionRepository.findAllByWalletIdAndType(walletId, transactionType)
                .forEach(transactionEntity -> {
                    if (transactionEntity.getCurrency().equals(CurrencyType.USD)) {
                        result.getAndUpdate(t -> t.add(transactionEntity.getAmount()
                                .multiply(currencyService.getCurrencyValue(CurrencyType.USD).getRate())));
                    } else if (transactionEntity.getCurrency().equals(CurrencyType.EUR)) {
                        result.getAndUpdate(t -> t.add(transactionEntity.getAmount()
                                .multiply(currencyService.getCurrencyValue(CurrencyType.EUR).getRate())));
                    } else {
                        result.getAndUpdate(t -> t.add(transactionEntity.getAmount()));
                    }
                });
        return result.get();
    }

    @Override
    public BigDecimal getWalletIncome(long walletId) {
        return countWalletTransactions(walletId, TransactionType.INCOME);
    }

    @Override
    public BigDecimal getWalletExpense(long walletId) {
        return countWalletTransactions(walletId, TransactionType.EXPENSE);
    }

    @Override
    public List<CategoryExpense> getCategoriesExpense() {
        return categoryRepository.getCategoryExpense(session.getUser().getId());
    }

    @Override
    public BigDecimal getWalletExpenseForCurrentMonth(long walletId) {
        return transactionRepository.getWalletExpenseForCurrentMonth(walletId, LocalDateTime.now()
                .withDayOfMonth(1)
                .truncatedTo(ChronoUnit.DAYS));
    }
}
