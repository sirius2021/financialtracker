package sirius.back.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.UserEntity;
import sirius.back.model.Session;
import sirius.back.repository.CategoryRepository;
import sirius.back.repository.TransactionRepository;
import sirius.back.service.CategoryService;
import sirius.back.utils.CategoryUtils;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final TransactionRepository transactionRepository;
    private final Session session;

    @Override
    public Iterable<CategoryEntity> getAll() {
        return categoryRepository.findAllByUserAndVisibility(session.getUser(), true);
    }

    @Override
    public void createCategory(CategoryEntity category) {
        categoryRepository.save(category.setUser(session.getUser()));
    }

    @Override
    public void updateCategory(CategoryEntity newCategory, long id) {
        categoryRepository.findByUserAndId(session.getUser(), id).ifPresent(oldCategory -> {
            if (newCategory.getName() != null)
                oldCategory.setName(newCategory.getName());
            if (newCategory.getType() != null)
                oldCategory.setType(newCategory.getType());
            if (newCategory.getPicture() != null)
                oldCategory.setPicture(newCategory.getPicture());
            if (newCategory.getColour() != null)
                oldCategory.setColour(newCategory.getColour());
            categoryRepository.save(oldCategory);
        });

    }

    @Override
    public void removeCategory(long id) {
        categoryRepository.findByUserAndId(session.getUser(), id).ifPresent(
                transactionRepository.countByCategoryId(id) != 0
                        ? obj -> categoryRepository.save(obj.setVisibility(false))
                        : categoryRepository::delete
        );
    }

    @Override
    public void createDefaultCategories(UserEntity user) {
        categoryRepository.saveAll(CategoryUtils.getDefaultCategories(user));
    }
}
