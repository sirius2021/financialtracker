package sirius.back.service;

import sirius.back.entity.CategoryEntity;
import sirius.back.entity.UserEntity;

public interface CategoryService {

    Iterable<CategoryEntity> getAll();

    void createCategory(CategoryEntity convert);

    void updateCategory(CategoryEntity convert, long id);

    void removeCategory(long id);

    void createDefaultCategories(UserEntity user);
}
