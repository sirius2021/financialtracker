package sirius.back.service;

import sirius.back.entity.UserEntity;

public interface UserService {

    boolean userExists(String email);

    UserEntity createUser(String email, String token);

    UserEntity findUserByEmail(String email);

    UserEntity setUserToken(UserEntity user, String token);
}
