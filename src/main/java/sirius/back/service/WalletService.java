package sirius.back.service;

import sirius.back.entity.UserEntity;
import sirius.back.entity.WalletEntity;
import sirius.back.entity.WalletExpense;

import java.util.List;

public interface WalletService {

    void createWallet(WalletEntity wallet);

    WalletEntity getWallet(long walletId);

    void removeWallet(long id);

    void editWallet(long id, WalletEntity wallet);

    List<WalletEntity> getWallets();

    void createDefaultWallet(UserEntity user);

    List<WalletExpense> getWalletMonthlyExpense(long walletId);
}
