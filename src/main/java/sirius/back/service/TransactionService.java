package sirius.back.service;

import sirius.back.entity.CategoryExpense;
import sirius.back.entity.TransactionEntity;
import sirius.back.entity.TransactionTypeSum;

import java.math.BigDecimal;
import java.util.List;

public interface TransactionService {

    void createTransaction(TransactionEntity transaction);

    List<TransactionEntity> getTransactions(int numberOfItems, int pageNumber, Long walletId);

    void updateTransaction(TransactionEntity transaction, Long id);

    void deleteTransaction(Long id);

    void deleteTransactionsByWalletId(long walletId);

    int countTransactionsByWalletId(Long walletId);

    List<TransactionTypeSum> getWalletIncomeExpense(long walletId);

    BigDecimal getWalletIncome(long walletId);

    BigDecimal getWalletExpense(long walletId);

    List<CategoryExpense> getCategoriesExpense();

    BigDecimal getWalletExpenseForCurrentMonth(long walletId);
}
