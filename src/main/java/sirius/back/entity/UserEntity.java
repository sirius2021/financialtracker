package sirius.back.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "user_seq", sequenceName = "user_seq")
@Table(name = "user_info")
public class UserEntity {

    @Id
    @GeneratedValue(generator = "user_seq")
    private Long id;

    private String email;
    private String token;

}
