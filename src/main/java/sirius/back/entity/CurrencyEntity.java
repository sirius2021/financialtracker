package sirius.back.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "currency_seq", sequenceName = "currency_seq")
@Table(name = "currency")
public class CurrencyEntity {

    @Id
    @GeneratedValue(generator = "currency_seq")
    private Long id;

    private String dynamics;
    private String name;
    private LocalDate date;
    private BigDecimal rate;
}
