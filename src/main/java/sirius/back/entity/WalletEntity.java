package sirius.back.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
@Table(name = "wallet")
@NamedNativeQuery(name ="wallet_monthly_expense_query",
        query = "select sum(amount) as amount, date_trunc('month', date) as date from transaction where date between ?1 and ?2 group by date_trunc('month', date);",
        resultSetMapping = "wallet_monthly_expense")
@SqlResultSetMapping(
        name = "wallet_monthly_expense",
        classes = @ConstructorResult(
                targetClass = WalletExpense.class,
                columns = {
                        @ColumnResult(name = "amount", type = BigDecimal.class),
                        @ColumnResult(name = "date", type = LocalDateTime.class)
                }
        )
)

public class WalletEntity {

    @Id
    @GeneratedValue(generator = "wallet_seq")
    private Long id;

    private String name;
    private String currency;
    @Column(name = "wallet_limit")
    private BigDecimal walletLimit;
    private Boolean visibility;

    @OneToOne(optional = false)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .append(currency)
                .append(walletLimit)
                .append(visibility)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof WalletEntity)) {
            return false;
        }

        WalletEntity walletEntity = (WalletEntity) obj;

        return new EqualsBuilder()
                .append(name, walletEntity.name)
                .append(currency, walletEntity.currency)
                .append(walletLimit, walletEntity.walletLimit)
                .append(visibility, walletEntity.visibility)
                .isEquals();
    }
}

