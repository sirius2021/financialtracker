package sirius.back.entity;

import lombok.Getter;
import lombok.Setter;
import sirius.back.constants.TransactionType;

import java.math.BigDecimal;

@Getter
@Setter
public class TransactionTypeSum {

    public TransactionTypeSum(String transactionType, BigDecimal amount) {
        this.amount = amount;
        this.type = TransactionType.valueOf(transactionType);
    }

    private TransactionType type;
    private BigDecimal amount;
}
