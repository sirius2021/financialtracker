package sirius.back.entity;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class CategoryExpense {

    public CategoryExpense(String category, BigDecimal amount) {
        this.category = category;
        this.amount = amount;
    }

    private BigDecimal amount;
    private String category;
}
