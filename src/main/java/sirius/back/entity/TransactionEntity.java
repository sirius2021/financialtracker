package sirius.back.entity;

import lombok.*;
import sirius.back.constants.CurrencyType;
import sirius.back.constants.TransactionType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(allocationSize = 1, name = "transaction_seq", sequenceName = "transaction_seq")
@Table(name = "transaction")
@NamedNativeQuery(name ="wallet_finance_query",
        query = " SELECT distinct transaction_type, sum(amount) over (partition by transaction_type) as amount from transaction tr WHERE wallet_id = ?1 group by amount, transaction_type;",
        resultSetMapping = "wallet_finance")
@SqlResultSetMapping(
        name = "wallet_finance",
        classes = @ConstructorResult(
                targetClass = TransactionTypeSum.class,
                columns = {
                        @ColumnResult(name = "transaction_type"),
                        @ColumnResult(name = "amount"),
                }
        )
)
public class TransactionEntity {

    @Id
    @GeneratedValue(generator = "transaction_seq")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallet_id")
    private WalletEntity wallet;

    private BigDecimal amount;

    @Column(name = "transaction_type")
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Enumerated(EnumType.STRING)
    private CurrencyType currency;

    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private CategoryEntity category;
}
