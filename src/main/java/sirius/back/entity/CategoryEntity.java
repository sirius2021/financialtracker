package sirius.back.entity;

import lombok.*;

import lombok.experimental.Accessors;
import sirius.back.constants.TransactionType;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@SequenceGenerator(allocationSize = 1, name = "category_seq", sequenceName = "category_seq")
@Table(name="category")
@NamedNativeQuery(name ="category_expense_query",
        query = "select c.name as category, sum(amount) as amount from transaction join category c on c.id = transaction.category_id join user_info ui on ui.id = c.user_id where user_id = ?1 and transaction_type = 'EXPENSE' group by c.name;",
        resultSetMapping = "category_expense_result")
@SqlResultSetMapping(
        name = "category_expense_result",
        classes = @ConstructorResult(
                targetClass = CategoryExpense.class,
                columns = {
                        @ColumnResult(name = "category", type = String.class),
                        @ColumnResult(name = "amount", type = BigDecimal.class),
                }
        )
)
public class CategoryEntity {

    @Id
    @GeneratedValue(generator = "category_seq")
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private UserEntity user;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="type")
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Column(name="picture")
    private String picture;

    @Column(name="colour")
    private Integer colour;

    @Builder.Default
    @Column(name = "visibility")
    private boolean visibility = true;
}
