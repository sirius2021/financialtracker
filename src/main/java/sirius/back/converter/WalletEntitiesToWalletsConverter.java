package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class WalletEntitiesToWalletsConverter {

    private final WalletEntityToWalletConverter walletEntityToWalletConverter;

    public List<Wallet> convert(List<WalletEntity> source) {
        return source.stream().collect(ArrayList::new,
                (wallets, walletEntity) -> wallets.add(walletEntityToWalletConverter.convert(walletEntity)),
                ArrayList::addAll);
    }

}
