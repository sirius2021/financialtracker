package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.CurrencyEntity;
import sirius.back.model.Currency;

@Component
@RequiredArgsConstructor
public class CurrencyEntityToCurrencyConverter {

    public Currency convert(CurrencyEntity source) {
        return Currency.builder()
                .name(source.getName())
                .rate(source.getRate())
                .dynamics(source.getDynamics())
                .build();
    }
}
