package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.CategoryExpense;
import sirius.back.model.CategoryExpenseModel;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CategoryExpenseToCategoryExpenseModelConverter {

    public List<CategoryExpenseModel> convert(List<CategoryExpense> source) {
        return source.stream()
                .map(categoryExpense -> CategoryExpenseModel.builder()
                        .category(categoryExpense.getCategory())
                        .amount(categoryExpense.getAmount())
                        .build())
                .collect(Collectors.toList());
    }
}
