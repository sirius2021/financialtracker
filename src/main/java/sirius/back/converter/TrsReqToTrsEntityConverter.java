package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.constants.CurrencyType;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.TransactionEntity;
import sirius.back.entity.WalletEntity;
import sirius.back.model.TransactionRequest;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class TrsReqToTrsEntityConverter {

    public TransactionEntity convert(TransactionRequest source, Long walletId) {
        CategoryEntity category = new CategoryEntity();
        category.setId(source.getCategoryId());

        WalletEntity wallet = new WalletEntity();
        wallet.setId(walletId);

        return TransactionEntity.builder()
                .wallet(wallet)
                .amount(source.getAmount())
                .type(source.getType())
                .category(category)
                .currency(source.getCurrency() == null ? CurrencyType.RUB : source.getCurrency())
                .date(source.getDate() == null ? LocalDateTime.now() : source.getDate())
                .build();
    }

    public TransactionEntity convert(TransactionRequest source) {
        CategoryEntity category = new CategoryEntity();
        if(source.getCategoryId() != null)
            category.setId(source.getCategoryId());
        else
            category = null;

        return TransactionEntity.builder()
                .amount(source.getAmount())
                .type(source.getType())
                .category(category)
                .currency(source.getCurrency())
                .date(source.getDate())
                .build();
    }
}
