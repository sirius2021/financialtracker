package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;

@Component
@RequiredArgsConstructor
public class WalletEntityToWalletConverter {

    public Wallet convert(WalletEntity source) {
        return Wallet.builder()
                .id(source.getId())
                .name(source.getName())
                .limit(source.getWalletLimit())
                .currency(source.getCurrency())
                .visibility(source.getVisibility())
                .build();
    }
}
