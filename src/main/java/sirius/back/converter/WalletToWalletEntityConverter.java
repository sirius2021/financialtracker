package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class WalletToWalletEntityConverter {

    public WalletEntity convert(Wallet source) {
        return WalletEntity.builder()
                .name(source.getName())
                .currency(source.getCurrency())
                .walletLimit(source.getLimit())
                .visibility(source.getVisibility())
                .build();
    }
}
