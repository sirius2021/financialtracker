package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.TransactionEntity;
import sirius.back.model.TransactionResponse;

@Component
@RequiredArgsConstructor
public class TrsEntityToTrsRespConverter {

    private final CategoryEntityToCategoryConverter catEntToCatConverter;

    public TransactionResponse convert(TransactionEntity source) {
        return TransactionResponse.builder()
                .id(source.getId())
                .amount(source.getAmount())
                .type(source.getType())
                .category(catEntToCatConverter.convert(source.getCategory()))
                .currency(source.getCurrency())
                .date(source.getDate())
                .build();
    }
}
