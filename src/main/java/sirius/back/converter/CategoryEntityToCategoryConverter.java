package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.CategoryEntity;
import sirius.back.model.Category;

@Component
@RequiredArgsConstructor
public class CategoryEntityToCategoryConverter {

    public Category convert(CategoryEntity categoryEntity) {
        return Category.builder()
                .id(categoryEntity.getId())
                .name(categoryEntity.getName())
                .type(categoryEntity.getType())
                .picture(categoryEntity.getPicture())
                .colour(categoryEntity.getColour())
                .build();
    }
}
