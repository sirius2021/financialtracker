package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.UserEntity;
import sirius.back.model.Category;

@Component
@RequiredArgsConstructor
public class CategoryToCategoryEntityConverter {

    public CategoryEntity convert(Category category) {
        return CategoryEntity.builder()
                .name(category.getName())
                .type(category.getType())
                .picture(category.getPicture())
                .colour(category.getColour())
                .build();
    }
}
