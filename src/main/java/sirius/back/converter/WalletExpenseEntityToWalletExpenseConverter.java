package sirius.back.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sirius.back.entity.WalletExpense;
import sirius.back.model.WalletExpenseModel;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class WalletExpenseEntityToWalletExpenseConverter {

    public List<WalletExpenseModel> convert(List<WalletExpense> source) {
        return source.stream()
                .map(walletExpense -> WalletExpenseModel.builder()
                        .amount(walletExpense.getAmount())
                        .date(walletExpense.getDate())
                        .build())
                .collect(Collectors.toList());

    }
}
