package sirius.back.constants;

public enum TransactionType {
    EXPENSE,
    INCOME
}
