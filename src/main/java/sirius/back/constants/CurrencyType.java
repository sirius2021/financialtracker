package sirius.back.constants;

public enum CurrencyType {
    RUB,
    USD,
    EUR
}
