package sirius.back.interceptor;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import sirius.back.entity.UserEntity;
import sirius.back.model.Session;
import sirius.back.service.CategoryService;
import sirius.back.service.UserService;
import sirius.back.service.WalletService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Component
@RequiredArgsConstructor
public class AuthInterceptor implements HandlerInterceptor {

    private final UserService userService;
    private final WalletService walletService;
    private final CategoryService categoryService;
    private final Session session;

    @Override
    @Transactional
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String email = request.getHeader("Email");
        String token = request.getHeader("Authorization").split(" ")[1];

        UserEntity user = userService.findUserByEmail(email);
        if (user == null) {
            user = userService.createUser(email, token);
            walletService.createDefaultWallet(user);
            categoryService.createDefaultCategories(user);
        } else {
            userService.setUserToken(user, token);
        }

        // save user info to 'request' bean
        session.setUser(user);

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
