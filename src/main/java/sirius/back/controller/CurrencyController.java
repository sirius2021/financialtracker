package sirius.back.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import sirius.back.constants.CurrencyType;
import sirius.back.converter.CurrencyEntityToCurrencyConverter;
import sirius.back.model.Currency;
import sirius.back.service.CurrencyService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;
    private final CurrencyEntityToCurrencyConverter currencyEntityToCurrencyConverter;

    @Operation(summary = "Метод для получения курса валюты")
    @GetMapping("/currency/{name}")
    public Currency getCurrency(@PathVariable(name = "name") CurrencyType name) {
        return currencyEntityToCurrencyConverter.convert(currencyService.getCurrencyValue(name));
    }

    @Operation(summary = "Метод для получения курсов валют")
    @GetMapping("/currencies")
    public List<Currency> getCurrencies() {
        return List.of(currencyEntityToCurrencyConverter.convert(currencyService.getCurrencyValue(CurrencyType.USD)),
                currencyEntityToCurrencyConverter.convert(currencyService.getCurrencyValue(CurrencyType.EUR)));
    }
}
