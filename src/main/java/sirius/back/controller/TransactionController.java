package sirius.back.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sirius.back.converter.TrsEntityToTrsRespConverter;
import sirius.back.converter.TrsReqToTrsEntityConverter;
import sirius.back.entity.TransactionEntity;
import sirius.back.model.TransactionRequest;
import sirius.back.model.TrsRespList;
import sirius.back.model.TransactionResponse;
import sirius.back.service.TransactionService;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;
    private final TrsReqToTrsEntityConverter trsToTrsEntConverter;
    private final TrsEntityToTrsRespConverter trsEntToTrsConverter;

    @Operation(summary = "Метод для создания транзакции")
    @PostMapping("/transaction")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransactionRequest transaction,
                                  @RequestParam(name = "walletId") Long walletId){
        transactionService.createTransaction(trsToTrsEntConverter.convert(transaction, walletId));
    }

    @Operation(summary = "Метод для получения всех транзакций выбранного кошелька пользователя")
    @GetMapping("/wallet/{walletId}/transactions")
    public TrsRespList getTransactions(@PathVariable(name = "walletId") Long walletId,
                                       @RequestParam(name = "dateFrom", required = false) String dateFrom,
                                       @RequestParam(name = "dateTo",  required = false) String dateTo,
                                       @RequestParam(name = "numberOfItems") int numberOfItems,
                                       @RequestParam(name = "pageNumber") int pageNumber){

        List<TransactionEntity> transactions = transactionService.getTransactions(numberOfItems, pageNumber, walletId);
        if(transactions != null) {
            List<TransactionResponse> transactionResponse = transactions.stream()
                    .collect(LinkedList::new,
                            (objects, objects2) -> objects.add(trsEntToTrsConverter.convert(objects2)),
                            LinkedList::addAll);

            return TrsRespList.builder()
                    .transactionList(transactionResponse)
                    .totalNumOfItems(transactionService.countTransactionsByWalletId(walletId))
                    .build();
        } else {
            return null;
        }
    }

    @Operation(summary = "Метод для редактирования транзакции")
    @PutMapping("transaction/{id}")
    public void updateTransaction(@PathVariable(name = "id") long id,
                                  @RequestBody TransactionRequest transaction) {
        transactionService.updateTransaction(trsToTrsEntConverter.convert(transaction), id);
    }

    @Operation(summary = "Метод для удаления транзакции")
    @DeleteMapping("transaction/{id}")
    public void deleteTransaction(@PathVariable(name = "id") long id) {
        transactionService.deleteTransaction(id);
    }
}