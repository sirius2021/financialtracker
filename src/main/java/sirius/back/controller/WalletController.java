package sirius.back.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sirius.back.converter.WalletEntityToWalletConverter;
import sirius.back.converter.WalletExpenseEntityToWalletExpenseConverter;
import sirius.back.converter.WalletToWalletEntityConverter;
import sirius.back.entity.TransactionTypeSum;
import sirius.back.entity.WalletEntity;
import sirius.back.model.Wallet;
import sirius.back.model.WalletExpenseModel;
import sirius.back.service.CurrencyService;
import sirius.back.service.TransactionService;
import sirius.back.service.WalletService;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class WalletController {

    private final WalletService walletService;
    private final TransactionService transactionService;
    private final CurrencyService currencyService;
    private final WalletEntityToWalletConverter walletEntityToWalletConverter;
    private final WalletToWalletEntityConverter walletToWalletEntityConverter;
    private final WalletExpenseEntityToWalletExpenseConverter walletExpenseEntityToWalletExpenseConverter;

    @Operation(summary = "Метод для создания кошелька")
    @PostMapping("/wallet")
    public void createWallet(@RequestBody @Valid Wallet wallet) {
        walletService.createWallet(walletToWalletEntityConverter.convert(wallet));
    }

    private Wallet setWalletLimitReaching(Wallet wallet) {
        if (wallet.getLimit().equals(BigDecimal.ZERO)) {
            return wallet.setIsLimitReached(false);
        }

        BigDecimal expense = transactionService.getWalletExpenseForCurrentMonth(wallet.getId());

        return wallet.setIsLimitReached(expense != null && expense.compareTo(wallet.getLimit()) >= 0);
    }

    private Wallet setWalletBalanceIncomeExpense(Wallet wallet) {
        BigDecimal income = transactionService.getWalletIncome(wallet.getId());
        BigDecimal expense = transactionService.getWalletExpense(wallet.getId());

        return wallet.setBalance(income.subtract(expense))
                .setIncome(income)
                .setExpense(expense);
    }

    @Operation(summary = "Метод для получения кошелька")
    @GetMapping("/wallet/{id}")
    public Wallet getWallet(@PathVariable(name = "id") long id) {
        WalletEntity walletEntity = walletService.getWallet(id);
        return setWalletLimitReaching(setWalletBalanceIncomeExpense(walletEntityToWalletConverter.convert(walletEntity)));
    }

    @Operation(summary = "Метод для удаления кошелька")
    @DeleteMapping("wallet/{id}")
    public void deleteWallet(@PathVariable(name = "id") long id) {
        walletService.removeWallet(id);
    }

    @Operation(summary = "Метод для редактирования кошелька")
    @PutMapping("wallet/{id}")
    public void editWallet(@PathVariable(name = "id") long id,
                           @RequestBody Wallet wallet) {
        walletService.editWallet(id, walletToWalletEntityConverter.convert(wallet));
    }

    @Operation(summary = "Метод для получения списка кошельков")
    @GetMapping("/wallets")
    public List<Wallet> getWallets() {
        return walletService.getWallets()
                .stream()
                .map(wallet -> setWalletLimitReaching(setWalletBalanceIncomeExpense(walletEntityToWalletConverter.convert(wallet))))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Метод для получения трат по кошельку за последний год")
    @GetMapping("/wallet/{wallet_id}/expense")
    public List<WalletExpenseModel> getWalletMonthlyExpense(@PathVariable(name = "wallet_id") long id) {
        return walletExpenseEntityToWalletExpenseConverter.convert(walletService.getWalletMonthlyExpense(id));
    }

}
