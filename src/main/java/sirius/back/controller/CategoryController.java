package sirius.back.controller;


import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import sirius.back.converter.CategoryEntityToCategoryConverter;
import sirius.back.converter.CategoryExpenseToCategoryExpenseModelConverter;
import sirius.back.converter.CategoryToCategoryEntityConverter;
import sirius.back.model.Category;
import sirius.back.model.CategoryExpenseModel;
import sirius.back.service.CategoryService;
import sirius.back.service.TransactionService;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;
    private final TransactionService transactionService;
    private final CategoryEntityToCategoryConverter categoryEntityToCategoryConverter;
    private final CategoryToCategoryEntityConverter categoryToCategoryEntityConverter;
    private final CategoryExpenseToCategoryExpenseModelConverter categoryExpenseToCategoryExpenseModelConverter;

    @Operation(summary = "Метод для получения списка всех категорий")
    @GetMapping("/categories")
    @ResponseStatus(HttpStatus.OK)
    public List<Category> getAllCategories() {
        List<Category> categories = new LinkedList();
        categoryService.getAll().forEach(obj -> categories.add(categoryEntityToCategoryConverter.convert(obj)));
        return categories;
    }

    @Operation(summary = "Метод для создания категории")
    @PostMapping("/category")
    @ResponseStatus(HttpStatus.CREATED)
    public void createCategory(@RequestBody @Valid Category category) {
        categoryService.createCategory(categoryToCategoryEntityConverter.convert(category));
    }

    @Operation(summary = "Метод для редактирования категории")
    @PutMapping("/category/{id}")
    public void updateCategory(@PathVariable(name = "id") long id,
                               @RequestBody @Valid Category category) {
        categoryService.updateCategory(categoryToCategoryEntityConverter.convert(category), id);
    }

    @Operation(summary = "Метод для удаления категории")
    @DeleteMapping("category/{id}")
    public void deleteCategory(@PathVariable(name = "id") long id) {
        categoryService.removeCategory(id);
    }

    @Operation(summary = "Метод для получения статистики расходов по категориям")
    @GetMapping("/categories/expense")
    @ResponseStatus(HttpStatus.OK)
    public List<CategoryExpenseModel> getCategoriesExpense() {
        return categoryExpenseToCategoryExpenseModelConverter.convert(transactionService.getCategoriesExpense());
    }

}
