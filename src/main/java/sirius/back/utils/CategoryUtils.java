package sirius.back.utils;

import sirius.back.constants.TransactionType;
import sirius.back.entity.CategoryEntity;
import sirius.back.entity.UserEntity;

import java.util.*;

public class CategoryUtils {

    public static Iterable<CategoryEntity> getDefaultCategories(UserEntity user) {
        return Arrays.asList(
                CategoryEntity.builder()
                        .user(user).name("Зарплата")
                        .type(TransactionType.INCOME)
                        .picture("icon_card")
                        .colour(0xff00cc00)
                        .build(),
                CategoryEntity.builder()
                        .user(user)
                        .name("Подарок")
                        .type(TransactionType.INCOME)
                        .picture("icon_gift")
                        .colour(0xffff00ff)
                        .build(),
                CategoryEntity.builder()
                        .user(user)
                        .name("Капитализация")
                        .type(TransactionType.INCOME)
                        .picture("icon_percent")
                        .colour(0xff00cc00)
                        .build(),
                CategoryEntity.builder()
                        .user(user)
                        .name("Супермаркет")
                        .type(TransactionType.EXPENSE)
                        .picture("icon_shop")
                        .colour(0xffff0000)
                        .build(),
                CategoryEntity.builder()
                        .user(user)
                        .name("Квартплата")
                        .type(TransactionType.EXPENSE)
                        .picture("icon_home")
                        .colour(0xff00cccc)
                        .build(),
                CategoryEntity.builder()
                        .user(user)
                        .name("Общественный транспорт")
                        .type(TransactionType.EXPENSE)
                        .picture("icon_bus")
                        .colour(0xff0099ff)
                        .build()
        );
    }
}
