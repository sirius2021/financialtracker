package sirius.back.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
public class Currency {
    @Schema(description = "Название валюты")
    private String name;
    @Schema(description = "Курс валюты")
    private BigDecimal rate;
    @Schema(description = "Динамика валюты (UP/DOWN/NONE)")
    private String dynamics;
}
