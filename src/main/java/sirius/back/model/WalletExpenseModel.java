package sirius.back.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
public class WalletExpenseModel {

    @Schema(description = "Трата")
    private BigDecimal amount;

    @Schema(description = "Месяц")
    private LocalDateTime date;

}
