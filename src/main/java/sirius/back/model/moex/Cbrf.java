package sirius.back.model.moex;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class Cbrf {
    private List<String> columns;
    private List<List<Object>> data;
}
