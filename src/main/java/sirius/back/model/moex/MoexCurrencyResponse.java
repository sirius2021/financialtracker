package sirius.back.model.moex;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class MoexCurrencyResponse {
    private Cbrf cbrf;
}
