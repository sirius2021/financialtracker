package sirius.back.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import sirius.back.constants.TransactionType;

import javax.validation.constraints.NotBlank;

@Builder
@Getter
@Setter
@Accessors(chain = true)
public class Category {

    @Schema(description = "Идентификатор категории")
    private long id;
    @NotBlank
    @Schema(description = "Название категории")
    private String name;
    @Schema(description = "Тип категории")
    private TransactionType type;
    @Schema(description = "Название иконки категории")
    private String picture;
    @Schema(description = "Цвет иконки категории")
    private Integer colour;

}
