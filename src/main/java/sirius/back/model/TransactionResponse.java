package sirius.back.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import sirius.back.constants.CurrencyType;
import sirius.back.constants.TransactionType;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Сущность транзакции
 */
@Builder
@Getter
@Setter
@Schema(description = "Транзакция")
public class TransactionResponse {

    private Long id;
    @NotBlank
    @Schema(description = "Сумма", required = true)
    private BigDecimal amount;
    @NotBlank
    @Schema(description = "Тип", required = true)
    private TransactionType type;
    @NotBlank
    @Schema(description = "Категория", required = true)
    private Category category;
    @Schema(description = "Валюта")
    private CurrencyType currency;
    @Schema(description = "Дата")
    private LocalDateTime date;
}
