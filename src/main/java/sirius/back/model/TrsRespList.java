package sirius.back.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class TrsRespList {

    private List<TransactionResponse> transactionList;
    private int totalNumOfItems;
}
