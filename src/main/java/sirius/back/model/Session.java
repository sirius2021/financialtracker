package sirius.back.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import sirius.back.entity.UserEntity;

@Component
@RequestScope
@Getter
@Setter
@NoArgsConstructor
public class Session {
    private UserEntity user;
}
