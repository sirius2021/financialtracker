package sirius.back.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@Accessors(chain = true)
public class Wallet {
    @Schema(description = "Идентификатор кошелька")
    long id;
    @NotBlank
    @Schema(description = "Название кошелька")
    String name;
    @NotBlank
    @Schema(description = "Валюта кошелька")
    String currency;
    @Schema(description = "Доход")
    BigDecimal income;
    @Schema(description = "Расход")
    BigDecimal expense;
    @Schema(description = "Баланс кошелька")
    BigDecimal balance;
    @Schema(description = "Видимость кошелька")
    Boolean visibility;
    @Schema(description = "Лимит затрат средств")
    BigDecimal limit;
    @Schema(description = "Достигнут ли лимит средств")
    Boolean isLimitReached;

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(name)
                .append(currency)
                .append(income)
                .append(expense)
                .append(balance)
                .append(visibility)
                .append(limit)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Wallet)) {
            return false;
        }

        Wallet wallet = (Wallet) obj;

        return new EqualsBuilder()
                .append(id, wallet.id)
                .append(name, wallet.name)
                .append(currency, wallet.currency)
                .append(income, wallet.income)
                .append(expense, wallet.expense)
                .append(balance, wallet.balance)
                .append(visibility, wallet.visibility)
                .append(limit, wallet.limit)
                .isEquals();
    }
}

