package sirius.back.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
public class CategoryExpenseModel {

    @Schema(description = "Расход")
    private BigDecimal amount;

    @Schema(description = "Катерогия")
    private String category;
}
