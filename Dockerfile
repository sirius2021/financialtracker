FROM openjdk:17-jdk-alpine

ARG JAR_FILE=build/libs/financialtracker.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","-XX:+UseSerialGC","-Xss512k","-XX:MaxRAM=256m","/app.jar"]

